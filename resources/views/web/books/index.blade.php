@extends('web.layouts.app')
@push('page-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('web_assets/styles/courses.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('web_assets/styles/courses_responsive.css') }}">
    @livewireStyles
@endpush
@section('main-body')
    @include('web.include.breadcrumbs',['currentName' => 'Books'])
    <!-- Books -->
    <div class="mt-5">
        <div class="container">
            @livewire('show-books')
        </div>
    </div>
@endsection
@push('page-js')
    @livewireScripts
@endpush
