@extends('web.layouts.app')
@push('page-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('web_assets/styles/courses.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('web_assets/styles/courses_responsive.css') }}">
    @livewireStyles
@endpush
@section('main-body')

    @include('web.include.breadcrumbs',['currentName' => 'Courses'])

    <!-- Courses -->

    <div class="courses">
        <div class="container">
            <div class="row">
                <!-- Courses Main Content -->
                <div class="col-lg-8">
                    @livewire('show-all-courses')
                </div>
                <!-- Courses Sidebar -->
                <div class="col-lg-4">
                    <div class="sidebar">

                        <!-- Categories -->
                        <div class="sidebar_section">
                            <div class="sidebar_section_title">Categories</div>
                            <div class="sidebar_categories">
                                <ul>
                                    <li><a href="{{ route('web.showBooks') }}">Books</a></li>
                                    <li><a href="#">Quiz</a></li>
                                    <li><a href="#">Paper</a></li>
                                </ul>
                            </div>
                        </div>

                        <!-- Latest Course -->
                        <div class="sidebar_section">
                            <div class="sidebar_section_title">Latest Courses</div>
                            <div class="sidebar_latest">

                                <!-- Latest Course -->
                                <div class="latest d-flex flex-row align-items-start justify-content-start">
                                    <div class="latest_image">
                                        <div><img src="{{ asset('web_assets/images/latest_1.jpg') }}" alt=""></div>
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_title"><a href="course.html">How to Design a Logo a Beginners
                                                Course</a></div>
                                        <div class="latest_price">Free</div>
                                    </div>
                                </div>

                                <!-- Latest Course -->
                                <div class="latest d-flex flex-row align-items-start justify-content-start">
                                    <div class="latest_image">
                                        <div><img src="{{ asset('web_assets/images/latest_2.jpg') }}" alt=""></div>
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_title"><a href="course.html">Photography for Beginners
                                                Masterclass</a></div>
                                        <div class="latest_price">$170</div>
                                    </div>
                                </div>

                                <!-- Latest Course -->
                                <div class="latest d-flex flex-row align-items-start justify-content-start">
                                    <div class="latest_image">
                                        <div><img src="{{ asset('web_assets/images/latest_3.jpg') }}" alt=""></div>
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_title"><a href="course.html">The Secrets of Body Language</a>
                                        </div>
                                        <div class="latest_price">$220</div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Gallery -->
                        <div class="sidebar_section">
                            <div class="sidebar_section_title">Instagram</div>
                            <div class="sidebar_gallery">
                                <ul class="gallery_items d-flex flex-row align-items-start justify-content-between flex-wrap">
                                    <li class="gallery_item">
                                        <div class="gallery_item_overlay d-flex flex-column align-items-center justify-content-center">
                                            +
                                        </div>
                                        <a class="colorbox" href="{{ asset('web_assets/images/gallery_1_large.jpg') }}">
                                            <img src="{{ asset('web_assets/images/gallery_1_large.jpg') }}" alt="">
                                        </a>
                                    </li>
                                    <li class="gallery_item">
                                        <div class="gallery_item_overlay d-flex flex-column align-items-center justify-content-center">
                                            +
                                        </div>
                                        <a class="colorbox" href="{{ asset('web_assets/images/gallery_2_large.jpg') }}">
                                            <img src="{{ asset('web_assets/images/gallery_2.jpg') }}" alt="">
                                        </a>
                                    </li>
                                    <li class="gallery_item">
                                        <div class="gallery_item_overlay d-flex flex-column align-items-center justify-content-center">
                                            +
                                        </div>
                                        <a class="colorbox" href="{{ asset('web_assets/images/gallery_3_lar') }}ge.jpg">
                                            <img src="{{ asset('web_assets/images/gallery_3.jpg') }}" alt="">
                                        </a>
                                    </li>
                                    <li class="gallery_item">
                                        <div class="gallery_item_overlay d-flex flex-column align-items-center justify-content-center">
                                            +
                                        </div>
                                        <a class="colorbox" href="{{ asset('web_assets/images/gallery_4_lar') }}ge.jpg">
                                            <img src="{{ asset('web_assets/images/gallery_4.jpg') }}" alt="">
                                        </a>
                                    </li>
                                    <li class="gallery_item">
                                        <div class="gallery_item_overlay d-flex flex-column align-items-center justify-content-center">
                                            +
                                        </div>
                                        <a class="colorbox" href="{{ asset('web_assets/images/gallery_5_lar') }}ge.jpg">
                                            <img src="{{ asset('web_assets/images/gallery_5.jpg') }}" alt="">
                                        </a>
                                    </li>
                                    <li class="gallery_item">
                                        <div class="gallery_item_overlay d-flex flex-column align-items-center justify-content-center">
                                            +
                                        </div>
                                        <a class="colorbox" href="{{ asset('web_assets/images/gallery_6_lar') }}ge.jpg">
                                            <img src="{{ asset('web_assets/images/gallery_6.jpg') }}" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- Tags -->
                        <div class="sidebar_section">
                            <div class="sidebar_section_title">Tags</div>
                            <div class="sidebar_tags">
                                <ul class="tags_list">
                                    <li><a href="#">creative</a></li>
                                    <li><a href="#">unique</a></li>
                                    <li><a href="#">photography</a></li>
                                    <li><a href="#">ideas</a></li>
                                    <li><a href="#">wordpress</a></li>
                                    <li><a href="#">startup</a></li>
                                </ul>
                            </div>
                        </div>

                        <!-- Banner -->
                        <div class="sidebar_section">
                            <div class="sidebar_banner d-flex flex-column align-items-center justify-content-center text-center">
                                <div class="sidebar_banner_background"
                                     style="background-image:url({{ asset('web_assets/images/banner_1.jpg') }})"></div>
                                <div class="sidebar_banner_overlay"></div>
                                <div class="sidebar_banner_content">
                                    <div class="banner_title">Free Book</div>
                                    <div class="banner_button"><a href="#">download now</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
@push('page-js')
    @livewireScripts
@endpush
