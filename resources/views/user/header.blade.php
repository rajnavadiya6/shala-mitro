<header class="header">
    <!-- Header Content -->
    <div class="header_container">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="header_content d-flex flex-row align-items-center justify-content-start">
                        <div class="logo_container">
                            <a href="{{ route('web.home') }}">
                                <div class="logo_text">Unic<span>at</span></div>
                            </a>
                        </div>
                        <nav class="main_nav_contaner ml-auto">
                            <ul class="main_nav">
                                <li class="{{ (url()->current() == route('web.home')) ? 'active' : '' }}">
                                    <a href="{{ route('web.home') }}">
                                        Home
                                    </a>
                                </li>

                                <li class=" {{ (url()->current() == route('web.about')) ? 'active' : '' }}">
                                    <a href="{{ route('web.about') }}">
                                        About
                                    </a>
                                </li>

                                <li class=" {{ (url()->current() == route('web.courses')) ? 'active' : '' }}">
                                    <a href="{{ route('web.courses') }}">
                                        Courses
                                    </a>
                                </li>

                                <li class=" {{ (url()->current() == route('web.blog')) ? 'active' : '' }}">
                                    <a href="{{ route('web.blog') }}">
                                        Blog
                                    </a>
                                </li>

                                <li class=" {{ (url()->current() == route('web.contact')) ? 'active' : '' }}">
                                    <a href="{{ route('web.contact') }}">
                                        Contact
                                    </a>
                                </li>
                                <li class=" {{ (url()->current() == route('userDashboardView')) ? 'active' : '' }}">
                                    <a href="{{ route('userDashboardView') }}">
                                        Dashbord
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
