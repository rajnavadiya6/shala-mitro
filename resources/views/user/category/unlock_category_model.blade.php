<!-- Modal area start -->
<div class="modal fade show model-lg" id="UnlockModal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            {{Form::open(['route' => ['categoryUnlock'], 'files' => true])}}
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h3 class="modal-title ">{{__('Unlock')}} <span class="text-primary cat-name"></span></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="unlock-area">
                        <div class="unlock-box">
                            <div class="unloack-header">
                                <ul>
                                    <li>{{__('You have total coin')}} :</li>
                                    <li>@if(isset(Auth::user()->userCoin->coin)) {{ Auth::user()->userCoin->coin }} @else
                                            0 @endif</li>
                                </ul>
                            </div>
                            <div class="unlock-body">
                                <h4>{{__('To unlock “')}}
                                    <span class="cat-name"></span> {{__('” Category')}}
                                </h4>
                                <span>
                                    <i class="fas fa-coins fa-2x mt-3 text-primary"></i>
                                    <span id="cat-coin" class="h4 mt-3 text-primary"></span>
                                </span>
                                <input type="hidden" id="cat-id" name="category_id">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="unlock-btn">
                        <button type="submit" class="">{{__('Unlock')}}</button>
                        <button type="button" class="btn close btn-cl" data-dismiss="modal"
                                aria-label="Close">{{__('Cancel')}}</button>
                    </div>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
<!-- Modal area start -->
