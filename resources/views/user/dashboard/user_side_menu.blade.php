<div class="user-side-menu">
    <ul class="submenu">
        <li class="active">
            <a href="{{route('userProfile')}}">
                <img src="{{asset('assets/user/images/header/img-1.png')}}" alt="">
                {{__('My Profile')}}</a>
        </li>
        <li>
            <a href="{{route('passwordChange')}}">
                <img src="{{asset('assets/user/images/header/img-2.png')}}" alt="">
                {{__('Change Password')}}
            </a>
        </li>
        <li>
            <a href="{{route('leaderBoards')}}">
                <img src="{{asset('assets/user/images/header/img-3.png')}}" alt="">
                {{__('Leader Board')}}
            </a>
        </li>
        <li>
            <a href="{{route('rewardSystem')}}">
                <i class="fa fa-graduation-cap"></i>
                {{__('Reward System')}}
            </a>
        </li>
        <li>
            <a href="{{route('buyCoin')}}">
                <img src="{{asset('assets/user/images/header/img-4.png')}}" alt="">{{__('Buy Coin')}}
            </a>
        </li>
        @if(allsetting('withdrawal_choice') && allsetting('withdrawal_choice') == 1)
            <li>
                <a href="{{route('withdrawalSystem')}}">
                    <i class="fa fa-money"></i>
                    {{__('Withdrawal system')}}
                </a>
            </li>
        @endif
        <li>
            <a href="{{route('buyCoinHistory')}}">
                <img src="{{asset('assets/user/images/header/img-5.png')}}" alt="">
                {{__('Buy Coin History')}}
            </a>
        </li>
        <li>
            <a href="{{route('referralSystem')}}">
                <i class="fa fa-link"></i> {{__('Referral System')}}
            </a>
        </li>
        <li>
            <a href="{{route('logOut')}}">
                <img src="{{asset('assets/user/images/header/logout.svg')}}" alt="">
                {{__('Logout')}}
            </a>
        </li>
    </ul>
</div>
