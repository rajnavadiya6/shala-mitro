<!DOCTYPE html>
<html lang="en">
@php
    $adm_setting = allsetting();
@endphp
<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Page Title -->
    <link rel="shortcut icon" type="image/png"
          href="@if(!empty(allsetting('favicon'))) {{ asset(path_image().allsetting('favicon')) }} @endif "/>
    <title>@yield('title','Quiz App | iTech')</title>
    @include('user.header_includes')
</head>

<body class="cat-b">

@include('user.header')
<div class="main-body">
    @yield('main-body')
</div>

@include('user.footer')

@include('user.scripts')

@yield('script')
</body>

</html>
