<?php

use Illuminate\Database\Seeder;

class BookCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        foreach (range(1, 10) as $index) {
            $input = [
                'name'   => 'ધોરણ '.$index,
                'status' => 1,
            ];
            $bookcategory = \App\Model\BookCategory::create($input);
            $input = [
                [
                    'name'             => 'અંગ્રેજી',
                    'seller_name'      => 'નવનીત',
                    'language'         => 'Gujarati',
                    'semester'         => 1,
                    'book_category_id' => $bookcategory->id,
                ],
                [
                    'name'             => 'હિન્દી',
                    'seller_name'      => 'નવનીત',
                    'language'         => 'English',
                    'semester'         => 1,
                    'book_category_id' => $bookcategory->id,
                ],
                [
                    'name'             => 'ગણિત',
                    'seller_name'      => 'નવનીત',
                    'language'         => 'Gujarati',
                    'semester'         => 2,
                    'book_category_id' => $bookcategory->id,
                ],
                [
                    'name'             => 'વિજ્ઞાન',
                    'seller_name'      => 'નવનીત',
                    'language'         => 'Gujarati',
                    'semester'         => 2,
                    'book_category_id' => $bookcategory->id,
                ],
            ];

            foreach ($input as $data) {
                \App\Model\Book::create($data);
            }
        }
    }
}
