<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function index(Request $request)
    {
        return view('web.courses');
    }

    public function showBooks()
    {
        return view('web.books.index');
    }
}
