<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Book;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class BookController extends Controller
{
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function allBooks(Request $request)
    {
        $data = ['success' => true, 'message' => __('all books retrieved Went wrong !')];
        $categoryId = $request->get('category');
        $semester = $request->get('semester');
        $medium = $request->get('medium');

        $books = Book::with('category');
        $books->when($categoryId, function (Builder $query) use ($categoryId) {
            $query->where('book_category_id', $categoryId);
        });
        $books->when($semester, function (Builder $query) use ($semester) {
            $query->where('semester', $semester);
        });
        $books->when($medium, function (Builder $query) use ($medium) {
            $query->where('language', $medium);
        });

        $data['data']['books'] = $books->get();

        return response()->json($data);
    }
    
}
