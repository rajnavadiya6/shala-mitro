<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateFAQRequest;
use App\Http\Requests\Admin\UpdateFAQRequest;
use App\Repositories\FAQRepository;
use Flash;
use Illuminate\Http\Request;
use Response;

class FAQController extends AppBaseController
{
    /** @var  FAQRepository */
    private $faqRepository;

    public function __construct(FAQRepository $faqRepo)
    {
        $this->fAQRepository = $faqRepo;
    }

    /**
     * Display a listing of the FAQ.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $data['pageTitle'] = 'FAQ';
        $data['menu'] = 'faqs';
        $data['faqs'] = $this->fAQRepository->paginate(10);

        return view('admin.faqs.index')
            ->with($data);
    }

    /**
     * Show the form for creating a new FAQ.
     *
     * @return Response
     */
    public function create()
    {
        $data['pageTitle'] = 'Add '.'FAQ';
        $data['menu'] = 'faqs';

        return view('admin.faqs.create')->with($data);
    }

    /**
     * Store a newly created FAQ in storage.
     *
     * @param  CreateFAQRequest  $request
     *
     * @return Response
     */
    public function store(CreateFAQRequest $request)
    {
        $input = $request->all();
        if (isset($input['image']) && ! empty($input['image'])) {
            $old_img = '';
            if (! empty($request->image)) {
                $old_img = $request->image;
            }
            $input['image'] = fileUpload($input['image'], path_common_image(), $old_img);
        }

        $faq = $this->fAQRepository->create($input);

        Flash::success('FAQ saved successfully.');

        return redirect(route('faqs.index'));
    }

    /**
     * Display the specified FAQ.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $data['pageTitle'] = 'FAQ';
        $data['menu'] = 'faqs';
        $data['faqs'] = $this->fAQRepository->find($id);

        if (empty($data['faqs'])) {
            Flash::error('FAQ not found');

            return redirect(route('faqs.index'));
        }

        return view('admin.faqs.show')->with($data);
    }

    /**
     * Show the form for editing the specified FAQ.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = 'Edit '.'FAQ';
        $data['menu'] = 'faqs';
        $data['faqs'] = $this->fAQRepository->find($id);

        if (empty($data['faqs'])) {
            Flash::error('FAQs not found');

            return redirect(route('faqs.index'));
        }

        return view('admin.faqs.edit')->with($data);
    }

    /**
     * Update the specified FAQ in storage.
     *
     * @param  int  $id
     * @param  UpdateFAQRequest  $request
     *
     * @return Response
     */
    public function update($id, UpdateFAQRequest $request)
    {
        $faq = $this->fAQRepository->find($id);

        if (empty($faq)) {
            Flash::error('FAQs not found');

            return redirect(route('faqs.index'));
        }
        $input = $request->all();
        if (isset($input['image']) && ! empty($input['image'])) {
            $old_img = '';
            if (! empty($request->image)) {
                $old_img = $request->image;
            }
            $input['image'] = fileUpload($input['image'], path_common_image(), $old_img);
        }

        $faq = $this->fAQRepository->update($input, $id);

        Flash::success('FAQs updated successfully.');

        return redirect(route('faqs.index'));
    }

    /**
     * Remove the specified FAQ from storage.
     *
     * @param  int  $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $faq = $this->fAQRepository->find($id);

        if (empty($faq)) {
            Flash::error('FAQs not found');

            return redirect(route('faqs.index'));
        }

        $this->fAQRepository->delete($id);

        Flash::success('FAQs deleted successfully.');

        return redirect(route('faqs.index'));
    }
}
