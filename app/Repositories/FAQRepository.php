<?php

namespace App\Repositories;

use App\Model\FAQ;

/**
 * Class FAQRepository
 * @package App\Repositories
 * @version November 11, 2020, 4:58 pm UTC
 */
class FAQRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'subject',
        'message',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FAQ::class;
    }
}
